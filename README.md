## installation
if you dont have any custom things in ohmyzsh dir
```bash
cd ~/.oh-my-zsh/custom
git clone git@gitlab.com:oaua/af-magic-alt.git
```

## activation
```bash
# in ~/.zshrc
ZSH_THEME="af-magic-alt"
```
