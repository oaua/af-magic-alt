# based on af-magic.zsh-theme
# remove code relative to python and mercurial 
# add kubeconfig display

kubinfo(){
  if [ -f "${KUBECONFIG}" ]; then
    BN=$(basename $KUBECONFIG)
    BN=${BN%.*}
    echo -n "${ZSH_THEME_KUBINFO_PROMPT_PREFIX}"
    echo -n "${BN}"
    echo -n "$ZSH_THEME_KUBINFO_PROMPT_SUFFIX"
  else
    return
  fi
}

# primary prompt: dashed separator, directory and vcs info
PS1="${FG[243]}\${(l.\$COLUMNS..-.)}%{$reset_color%}
${FG[032]}%~\$(git_prompt_info)\$(kubinfo) ${FG[105]}%(!.#.»)%{$reset_color%} "
PS2="%{$fg[red]%}\ %{$reset_color%}"

# right prompt: return code, virtualenv and context (user@host)
RPS1="%(?..%{$fg[red]%}%? ↵%{$reset_color%})"
RPS1+=" ${FG[243]}%n@%m%{$reset_color%}"

# git settings
ZSH_THEME_KUBINFO_PROMPT_PREFIX=" ${FG[075]}[${FG[078]}"
ZSH_THEME_KUBINFO_PROMPT_CLEAN=""
ZSH_THEME_KUBINFO_PROMPT_DIRTY="${FG[214]}*%{$reset_color%}"
ZSH_THEME_KUBINFO_PROMPT_SUFFIX="${FG[075]}]%{$reset_color%}"

ZSH_THEME_GIT_PROMPT_PREFIX=" ${FG[075]}(${FG[078]}"
ZSH_THEME_GIT_PROMPT_CLEAN=""
ZSH_THEME_GIT_PROMPT_DIRTY="${FG[214]}*%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="${FG[075]})%{$reset_color%}"
